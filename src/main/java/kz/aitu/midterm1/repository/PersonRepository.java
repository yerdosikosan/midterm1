package kz.aitu.midterm1.repository;

import kz.aitu.midterm1.model.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository  extends CrudRepository<Person, Long> {
}
