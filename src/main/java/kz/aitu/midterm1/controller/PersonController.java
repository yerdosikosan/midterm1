package kz.aitu.midterm1.controller;

import kz.aitu.midterm1.model.Person;
import kz.aitu.midterm1.service.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class PersonController {
    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/api/person")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(personService.getAll());
    }

    @GetMapping("/api/person/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(personService.getByID(id));
    }

    @PostMapping("/api/person")
    public ResponseEntity<?> save(@RequestBody Person person){
        return ResponseEntity.ok(personService.create(person));
    }

    @PutMapping("/api/person")
    public ResponseEntity<?> update(@RequestBody Person person){
        return ResponseEntity.ok(personService.create(person));
    }

    @DeleteMapping("/api/person/{id}")
    public void delete(@PathVariable Long id){
        personService.delete(id);
    }
}
